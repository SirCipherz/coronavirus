#!/bin/node
const Discord = require('discord.js');
const fs = require('fs')
const TOKEN = fs.readFileSync("/etc/le-coronavirus/token", 'utf8')

// Check for all user if they have an account

function CheckAndCreateAccount(id, verbose=false){
    let rawdata = fs.readFileSync('/etc/le-coronavirus/users.json');
    let users   = JSON.parse(rawdata);

    if (typeof users[id] === 'undefined'){
        // Create the account
        users[id] = {
            id: id,
            nom: '',
            classe: '',
            twitter: '',
            insta: '',
            snap: ''
        }
        console.log('Création du compte')
        let data = JSON.stringify(users);
        fs.writeFileSync('/etc/le-coronavirus/users.json', data);
    }
    else if (verbose == true){
        console.log('Le compte existe déjà')
    }
}

function formaterLaDate(timestamp){
    if (timestamp/3600 < 1){
        return Math.floor(timestamp/60) + " minutes";
    }
    else if (timestamp/86400 < 1){
        return Math.floor(timestamp/3600) + " heures";
    }
    else{
        return Math.floor(timestamp/86400) + " jours";
    }
}

function showThisValue(value){
    if (value === ''){
        return false;
    }
    else{
        return true;
    }
}

//console.log(TOKEN);

const client = new Discord.Client();

client.once('ready', () => {
    console.log('Ready!');
    for (let user of client.guilds.cache.first().members.cache){
        CheckAndCreateAccount(user[0]);
    }
});

client.login(TOKEN);

client.on('guildMemberAdd', member => {
    member.send("Bienvenue gros");
    for (let user of client.guilds.cache.first().members.cache){
        CheckAndCreateAccount(user[0], true);
    }
});

client.on('message', message => {
    let content = message.content;
    let author  = message.author;
    let channel = message.channel;

    if (content === '.ping'){
        channel.send("https://youtu.be/hAe6vaQSARg");
    }
    
    if (content.split(' ')[0] === '.profil'){
        if (content.split(' ').length == 1){
            let user    = author;
            let member  = author.presence.member;

            let rawdata = fs.readFileSync('/etc/le-coronavirus/users.json');
            let infos   = JSON.parse(rawdata);

            let perso   = infos[user.id];

            let info    = new Discord.MessageEmbed()
                .setColor(member.displayHexColor) // Utilise la couleur de l'utilisateur
                .setTitle('Votre Profil')
                .setURL('https://motherfuckingwebsite.com/')
                .setDescription(' ')
                .setThumbnail("https://cdn.discordapp.com/avatars/" + user.id + "/" + user.avatar + ".png")
                .addField("Pseudo", user.username, false)
            if (showThisValue(perso['nom'])){info.addField("Nom", perso['nom'], false)}
            if (showThisValue(perso['classe'])){info.addField("Classe", perso['classe'], false)}
            if (showThisValue(perso['snap'])){info.addField("Snap", perso['snap'], false)}
            if (showThisValue(perso['insta'])){info.addField("Insta", perso['insta'], false)}
            if (showThisValue(perso['twitter'])){info.addField("Twitter", perso['twitter'], false)}
            channel.send(info);
        }
        else if ((content.split(' ').length >= 2) && (content.split(' ')[1].slice(0,3) === '<@!')){
            let user    = message.mentions.members.first().user;
            //let member  = user.presence.member;

            let rawdata = fs.readFileSync('/etc/le-coronavirus/users.json');
            let infos   = JSON.parse(rawdata);

            let perso   = infos[user.id];

            let info    = new Discord.MessageEmbed()
                .setColor('#FFFFFF')//member.displayHexColor) // Utilise la couleur de l'utilisateur
                .setTitle('Profil de ' + user.username)
                .setURL('https://motherfuckingwebsite.com/')
                .setDescription(' ')
                .setThumbnail("https://cdn.discordapp.com/avatars/" + user.id + "/" + user.avatar + ".png")
                .addField("Pseudo", user.username, false)

            if (showThisValue(perso['nom'])){info.addField("Nom", perso['nom'], false)}
            if (showThisValue(perso['classe'])){info.addField("Classe", perso['classe'], false)}
            if (showThisValue(perso['snap'])){info.addField("Snap", perso['snap'], false)}
            if (showThisValue(perso['insta'])){info.addField("Insta", perso['insta'], false)}
            if (showThisValue(perso['twitter'])){info.addField("Twitter", perso['twitter'], false)}
            channel.send(info);
        }
        else if ((content.split(' ').length >= 4) && (content.split(' ')[1] === 'set') && (['nom','classe','snap','insta','twitter'].includes(content.split(' ')[2]))){
            let rawdata = fs.readFileSync('/etc/le-coronavirus/users.json');
            let users   = JSON.parse(rawdata);
            let gars    = users[author.id];
            
            gars[content.split(' ')[2]] = content.split(' ').slice(3,content.split(' ').lengh)

            let data = JSON.stringify(users);
            fs.writeFileSync('/etc/le-coronavirus/users.json', data);
            channel.send("J'ai modifié cette info");
        }
        else if (content === '.profil help'){
            channel.send("AIDE\n```\nAIDE\n.profil                     : affiche ton profil\n.profil help                : affiche se message\n.profil @qqn                : affiche le profil de @qqn\n.profil set [info] <valeur> : change l'info spécifiée dans [info]\n\n[info] :\nnom\nclasse\nsnap\ninsta\ntwitter\n(Ne pas mettre de crochet)\n\n<valeur> : ne pas mettre de < ou de >\n```")
        }
    }
});
